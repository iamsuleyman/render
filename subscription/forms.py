from django import forms
from django.core.mail import send_mail

from .models import Application, Follower


class ApplicationForm(forms.ModelForm):
    email = forms.EmailField(required=True)
    message = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Application
        fields = ('email', 'message')

    def send_notification(self):
        send_mail('Application', 'Your application accepted and will be moderated', 'isweetter@gmail.com',
                  [self.cleaned_data['email']], fail_silently=True)


class SubscribeForm(forms.ModelForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = Follower
        fields = ('email',)
