# -*- coding: UTF-8 -*-
from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib import messages

from .forms import ApplicationForm, SubscribeForm
from .models import Follower


def unsubscribe(request):
    if 'email' in request.GET and 'token' in request.GET:
        Follower.unsubscribe_user(request.GET['email'], request.GET['token'])
    return redirect('/')


def message(request):
    return render(request, 'subscription/message.html')


class ApplicationView(View):
    form_class = ApplicationForm
    template_name = 'subscription/application.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {
            'form': form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.REQUEST)
        if form.is_valid():
            form.save()
            form.send_notification()
            messages.add_message(request, messages.INFO, 'Your application accepted, wait moderation!')

            return redirect('/subscription/message/')

        return self.get(request, *args, **kwargs)


class SubscribeView(View):
    form_class = SubscribeForm
    template_name = 'subscription/subscribe.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {
            'form': form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.REQUEST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'You have been subscribed to news successfully!')
            return redirect('/subscription/message/')

        return self.get(request, *args, **kwargs)
