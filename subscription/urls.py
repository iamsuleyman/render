from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
                       url(r'^application/$', views.ApplicationView.as_view(), name='application'),
                       url(r'^subscribe/$', views.SubscribeView.as_view(), name='subscribe'),
                       url(r'^message/$', views.message, name='message'),
                       url(r'^unsubscribe/$', views.unsubscribe, name='unsubscribe'),
                       )
