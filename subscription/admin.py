from django.contrib import admin
from django.core.mail import send_mail

from .models import Application, Follower


def action_approve(modeladmin, request, queryset):
    queryset.update(is_moderated=True)
    users = Follower.objects.filter(is_mailing=True)
    for q in queryset:
        send_mail('Application', q.message, q.email, [u.email for u in users], fail_silently=True)


action_approve.short_description = "Approve"


class FollowersAdmin(admin.ModelAdmin):
    fields = ['email']
    list_display = ('email', 'is_mailing', 'created')


class ApplicationsAdmin(admin.ModelAdmin):
    fields = ['email', 'message', 'is_moderated']
    list_display = ('email', 'is_moderated', 'created')
    save_on_top = True
    actions = [action_approve]


admin.site.register(Application, ApplicationsAdmin)
admin.site.register(Follower, FollowersAdmin)
