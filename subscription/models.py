from uuid import uuid1

from django.db import models


class Follower(models.Model):
    email = models.EmailField(null=False, unique=True)
    created = models.DateTimeField(auto_now=True)
    is_mailing = models.BooleanField(default=True)  # if True, then we can send emails
    token = models.CharField(default=str(uuid1()), max_length=255)

    def __str__(self):
        return self.email

    @staticmethod
    def unsubscribe(email, token):
        try:
            user = Follower.objects.get(email=email, token=token)
            user.is_mailing = False
            user.token = str(uuid1())
            user.save()
        except Follower.DoesNotExist:
            pass


class Application(models.Model):
    email = models.EmailField()
    message = models.TextField(null=False)
    is_moderated = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True)
